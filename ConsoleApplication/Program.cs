﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyApp.Entities;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new BookTestEntities())
            {
                try
                {
                    // First create an Author
                    var author = new Author()
                    {
                        FirstName = "Jim",
                        LastName = "Bob"
                    };
                    // Next create a Book
                    var book = new Book()
                    {
                        Title = "Time for Amanda to get a new jobby job",
                        Author = author // referencing the author we created above
                    };

                    db.Books.Add(book);
                    db.SaveChanges();  // Writes to the Database.

                    // Ok, now lets print out the info from the DB
                    var bookFromDB = db.Books
                        .Include("Author")
                        .Where(b => b.Title.IndexOf("Amanda") != -1)
                        .FirstOrDefault();
                    // Print out DB stuff to the console
                    Console.WriteLine("Book Title: " + bookFromDB.Title + " with an Author: " + 
                        bookFromDB.Author.FirstName + " " + bookFromDB.Author.LastName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                }
                    
            }
            // Wait for the user to click 'enter' before closing the console window, so you can actually
            // see the output
            Console.ReadLine();
        }
    }
}
