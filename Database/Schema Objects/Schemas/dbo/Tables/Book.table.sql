﻿CREATE TABLE [dbo].[Book] (
    [BookId]   INT            IDENTITY (1, 1) NOT NULL,
    [Title]    NVARCHAR (255) NULL,
    [AuthorId] INT            NOT NULL
);

