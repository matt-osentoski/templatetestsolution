﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyApp.Entities;
using Web.Models;

namespace Web.Controllers
{
    public class BookController : Controller
    {
        private BookTestEntities db = new BookTestEntities();

        //
        // GET: /Book/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Book/Details/5

        public ActionResult Details(int id)
        {
            // Grab the book from the DB by its 'id'
            // (NOTE: The reason I use 'FirstOrDefault' here instead of just 'First' is that
            // 'FirstOrDefault' will return 'null' if the result is empty, 'First' will throw an exception.
            var book = db.Books
                .Include("Author")
                .FirstOrDefault(b => b.BookId == id);

            // If no books were found, redirect to the /Book/Index page.
            if (book == null)
                RedirectToAction("Index");

            // Create a new BookModel Object since our View is strongly typed.
            // This class acts as a wrapper around the Entity framework class.  Again, there are a number of different
            // ways and styles to accomplish the same thing, but I prefer this approach.
            var model = new BookModel();
            model.SelectedBook = book;

            // Send to the view (Details.cshtml) with the strongly typed BookModel object loaded with our book from 
            // the database.
            return View(model); 
        }

        //
        // GET: /Book/Create
        public ActionResult Create()
        {
            // We need a list of Authors for the dropdown box
            // (NOTE: Normally, you would put the code to grab an author into a private method so that other 
            // methods in the controller can access it from one place, but I'll just add it here to 
            // make the code easier to follow.)
            var authors = db.Authors
                .OrderBy(a => a.FirstName).ToList();
            // If you have to perform functions like 'ToString' in the 'SelectListItem' below, you have to separate out
            // the 'Select' statement after LINQ to Entities has run, otherwise it will throw an error.  This is why 
            // I'm explicitly calling 'ToList()' above, which finalizes the call to the DB
            var authorSelectListItems = authors
                .Select(s =>  new SelectListItem { Value = s.AuthorId.ToString(), Text = s.FirstName + " " + s.LastName  })
                .ToList();

            var model = new BookModel();
            model.Authors = authorSelectListItems;  // Add the authors to the BookModel and send it on to the page.

            return View(model);
        } 

        //
        // POST: /Book/Create
        [HttpPost]
        public ActionResult Create(BookModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //If the validations are valid, Add the new book
                    var author = db.Authors.FirstOrDefault(a => a.AuthorId == model.AuthorId);
                    var book = new Book()
                    {
                        Title = model.Title,
                        Author = author
                    };
                    db.Books.Add(book);
                    db.SaveChanges();  // Saves the book to the database

                    return RedirectToAction("Index");
                }
                else
                {
                    return View(model); // If a validation failed, return to the view with the existing model.
                }          
            }
            catch
            {
                return View(model);
            }
        }
        
        //
        // GET: /Book/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Book/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Book/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Book/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
