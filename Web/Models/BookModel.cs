﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyApp.Entities;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class BookModel
    {
        public List<Book> Books { get; set; }

        // Authors is used to build a dropdown
        // (NOTE: 'SelectListItem' is a helper object in MVC for creating dropdowns.)
        public List<SelectListItem> Authors { get; set; }

        // Use this property when displaying a single book
        public Book SelectedBook { get; set; }

        // Now mimic the book fields from Entity Framework.  These fields will be used in
        // our forms.

        public int BookId { get; set; }
        // The 'Required' attribute below is used for form validations
        // MVC has a large number of these and you can even grab 3rd party
        // validators from Nuget to make validation easy.
        [Required]
        public string Title { get; set; }
        public int AuthorId { get; set; }
    }
}